# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from client.oauth2.views import IdentityProviderRedirectView
# from client.views import home, api_test
from . import admin
from django.views.generic import RedirectView
from .views_fehler import error_403, error_404  # , error_500 (bei 500 funktionierte email nicht mehr)

from client.oauth2.views import IdentityProviderRedirectView
from client.views import home, api_test
from . import admin

urlpatterns = [
    path('', home, name='home'),
    path('api/v1/test/', api_test, name='api_test'),
    path('about/', RedirectView.as_view(url=settings.ABOUT, permanent=False), name='about'),
    path('auth_profile/', IdentityProviderRedirectView.as_view(uri_name='profile_uri', permanent=False),
         name='auth_profile'),
    path('oauth2/', include('client.oauth2.urls')),
    path('admin/', admin.site.urls),
]

handler403 = error_403
handler404 = error_404
