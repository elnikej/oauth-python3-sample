from base64 import urlsafe_b64decode, urlsafe_b64encode
from json import dumps
from logging import getLogger

logger = getLogger(__name__)


def _urlsafe_b64encode(raw_bytes):
    s = urlsafe_b64encode(raw_bytes)
    return s.rstrip(b'=')


def _urlsafe_b64decode(b64string):
    # Guard against unicode strings, which base64 can't handle.
    b64string = b64string.encode('ascii')
    padded = b64string + b'=' * (4 - len(b64string) % 4)
    return urlsafe_b64decode(padded)


def _json_encode(data):
    return dumps(data, separators=(',', ':'))
