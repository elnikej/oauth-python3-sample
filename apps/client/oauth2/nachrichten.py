from django.core.mail import send_mail
from django.conf import settings
from os import path
region = path.join(settings.BRAND)


# zum versenden von Nachrichten (zur Zeit per Email) an admins oder Verwalter


def senden(variante, nachricht):
    """
    :param variante: Variante des versendens
    :param nachricht: Text (z.Bsp Fehlerpunkt
    :return: None
    """
    # für Regionen
    # für Regionen
    emailtolist = ['mail@pemail.org']
    if region == 'Test':
        aufruf = '/verwaltung/users/'
    else:
        aufruf = 'unbekannt'
    if variante == '1':
        betreff = 'Neuer Benutzer in ' + region
        body = 'Ein neuer Benutzer hat sich in ' + aufruf + ' angemeldet. Benutzername :' + nachricht
    elif variante == '2':
        betreff = 'Deaktivierter (oder nicht zugeordneter) Benutzer in ' + region
        body = 'Ein nicht verknüpfter Benutzer hat sich in ' + aufruf + ' angemeldet. Benutzername :' + nachricht
    elif variante == '3':
        betreff = 'User nicht in dw-connect mit BuHaWeb verbunden'
        body = 'Benutzer: ' + nachricht
    else:
        betreff = 'unbekannte variante'
        body = 'unbekannte variante ' + region
    try:
        send_mail(betreff, body, 'von-email@email.org', emailtolist, fail_silently=False, )
    except ConnectionRefusedError:
        pass  # für Testsystem
