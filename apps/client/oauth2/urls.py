from django.urls import path
from .views import UserInfoClientView, UserInfoView, login, logout, SessionView, TokenInfoView
app_name = 'client_oauth2'


# mysite_oauth:
urlpatterns = [
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('userinfo/me/', UserInfoView.as_view(), name='userinfo_me'),
    path('tokeninfo/me/', TokenInfoView.as_view(), name='tokeninfo_me'),
    path('userinfo/client/', UserInfoClientView.as_view(), name='userinfo_client'),
    path('userinfo/<slug:uuid>/', UserInfoView.as_view(), name='userinfo'),
    path('session/', SessionView.as_view(template_name="oauth2/session.html"), name='session'),
]
